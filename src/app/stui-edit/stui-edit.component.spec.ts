import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StuiEditComponent } from './stui-edit.component';

describe('StuiEditComponent', () => {
  let component: StuiEditComponent;
  let fixture: ComponentFixture<StuiEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StuiEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StuiEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
