import { ComponentFixture, TestBed } from '@angular/core/testing';

import { STUIFindComponent } from './stui-find.component';

describe('STUIFindComponent', () => {
  let component: STUIFindComponent;
  let fixture: ComponentFixture<STUIFindComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ STUIFindComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(STUIFindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
